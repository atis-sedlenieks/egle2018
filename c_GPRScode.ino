void startGSM()
{
	// /////////START GSM/////////////
	// After starting the modem with GSM.begin()
	// attach the shield to the GPRS network with the APN, login and password
	Serial.println(F("Starting GSM"));
	//gsmSerial.write("AT+GSN\r\n");

	if (gsmAccess.begin(PINNUMBER) == GSM_READY)
	{
		Serial.println("PIN correct = GSM READY");
	}
	else
	{
		Serial.println("PIN error");
		delay(500);
	}
	if (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY)
	{
		Serial.println("APN correct = GPRS READY");
	}
	else
	{
		Serial.println("APN error");
		delay(500);
	}
	// /////////START GSM/////////////
	delay(500);
}

// /////////check ON OFF status/////////
void OnOff(char connect_path[], float voltage, float temperature, int BTmax)
{
	// if you get a connection, report back via serial:
	Serial.println(F(""));
	Serial.println(F("Checking ON OFF status..."));
	Serial.println(F("Connecting..."));
	if (client.connect(server, port))
	{
		client.print("GET ");
		if (voltage == 0)
		{
			client.print(connect_path);
			Serial.println(connect_path);
		}

		if (voltage > 0)
		{
			client.print(connect_path);
			client.print("?voltage=");
			client.print(voltage);
			client.print("&temp=");
			client.print(temperature);
			client.print("&BT=");
			client.print(BTmax);
			//
			Serial.print(connect_path);
			Serial.print("?voltage=");
			Serial.print(voltage);
			Serial.print("&temp=");
			Serial.print(temperature);
			Serial.print("&BT=");
			Serial.println(BTmax);
		}
		client.println(" HTTP/1.1");
		client.print("Host: ");
		client.println(server);
		client.println("Connection: close");
		client.println();
	}
	else
	{
		// if you didn't get a connection to the server:
		Serial.println(F("!!!!!!!!!!!!!!!!!!!!!!!!!connection failed"));
		Serial.println(F("Restarting GSM module"));
		startGSM();
	}

}

// ///////function for connecting to web page START //////////////
void updateHashtag()
{
	// if you get a connection, report back via serial:
	Serial.println(F(""));
	Serial.println(F("Updating HT..."));
	Serial.println(F("Connecting..."));
	if ((client.available() > 0))
	{
	client.flush();// flush before connect
	}
	if (client.connect(server, port))
	{
		Serial.println("Searching for hashtag: " + hashtag);
		client.print("GET ");
		client.print(path + hashtag);
		client.println(" HTTP/1.1");
		client.print("Host: ");
		client.println(server);
		client.println("Connection: close");
		client.println();
		if (client.connected())
		{
			Serial.println(F("CLIENT IS CONNECTED"));
			HTparse = true; // start ht parser
		}
	}
	else
	{
		// if you didn't get a connection to the server:
		Serial.println(F("!!!!!!!!!!!!!!!!!!!!!!!!!!!connection failed"));
		Serial.println(F("Restarting GSM module"));
		HTparse = false; // disable ht parser
		startGSM();
	}
}

// ////////////////parser start'/////////////
// ////////////////parser start'/////////////
unsigned long GPRSparser()
{
	// hashtag_result_int = 0;
	// //Serial.println("HT client connected.");
	fullDataReceived = false;
	if ((client.available() > 0))
	{
		// && (client.connected())
		// Serial.println(F("GPRS client conected."));
		cP = client.read();
		// Serial.print(cP);
		if (cP == '#')
		{
			writeBool = true;
			// Serial.println("I see #");
		}
		if (writeBool == true)
		{
			// Serial.print(c );
			hashtag_buffer[HTindex] = cP; // appending characters to buffer array
			HTindex++;
			if (cP == '\n')
			{
				hashtag_buffer[0] = '0'; // removing # character
				hashtag_buffer[HTindex - 2] = '\0';
				writeBool = false;
				fullDataReceived = true;
			}
			// else
			// if (c != '\r') {
			// hashtag_buffer[index++] = c; //CHECK IF YOU DON'T OVERFLOW THE BUFFER
			// }
			if (fullDataReceived)
			{
				fullDataReceived = false;
				// cP = ' ';
				// client.flush();
				// client.stop();
				HTindex = 0; // reset hashtag_buffer
				hashtag_result_int = atol(hashtag_buffer);
				// poundSignReceived = false;
				Serial.print(F("Hashtag_result_int: "));
				Serial.println(hashtag_result_int);
				HTparse = false; // end ht parser
				return hashtag_result_int;
			}
		}
	}
}

// ///////function for connecting to web page END //////////////