// ////////ARRAYS/////////////
void whiteArrayGEN()
{
	// generate white LED array
	for (channel = 3; channel <= (NUM_TLCS * 16) - 1; channel += directionChannel)
	{
		whiteArray[arrayIndex] = channel;
		arrayIndex++;
		// Serial.println(channel);
	}
	arrayIndex = 0;
}

void REDArrayGEN()
{
	// generate red LED array
	for (channel = 2; channel <= (NUM_TLCS * 16) - 1; channel += directionChannel)
	{
		REDArray[arrayIndex] = channel;
		arrayIndex++;
	}
	arrayIndex = 0;
}

void GREENarrayGEN()
{
	// generate green LED array
	for (channel = 0; channel <= (NUM_TLCS * 16) - 1; channel += directionChannel)
	{
		GREENarray[arrayIndex] = channel;
		arrayIndex++;
	}
	arrayIndex = 0;
}

void BLUEarrayGEN()
{
	// generate blue LED array
	for (channel = 1; channel <= (NUM_TLCS * 16) - 1; channel += directionChannel)
	{
		BLUEarray[arrayIndex] = channel;
		arrayIndex++;
	}
	arrayIndex = 0;
}

// //TEST CHANNELS
// //TEST CHANNELS
// 
// ////////////////////
// //////////////////////
void randomColorFades()
{
	if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	{
		unsigned int RandomChannel = random(0, totalChannelSum);
		// 
		if (!tlc_isFading(RandomChannel))
		{
			uint16_t duration = 1000;
			uint32_t startMillis = millis() + 50;
			uint32_t endMillis = startMillis + duration;
			tlc_addFade(RandomChannel, 0, maxValue, startMillis, endMillis);
			tlc_addFade(RandomChannel, maxValue, 0, endMillis, endMillis + duration);
		}
	}
	// tlc_updateFades();
}

void RandomWhiteColorFades()
{
	if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	{
		unsigned int RandomWChannel = whiteArray[random(0, OneChannelTotalSum)];
		// 
		if (!tlc_isFading(RandomWChannel))
		{
			// uint16_t duration = random(100, 1000);
			uint16_t duration = 1000;
			uint32_t startMillis = millis() + 50;
			uint32_t endMillis = startMillis + duration;
			tlc_addFade(RandomWChannel, 0, maxValue, startMillis, endMillis); // FADE UP
			tlc_addFade(RandomWChannel, maxValue, 0, endMillis, endMillis + duration); // FADE DOWN
		}
	}
}

void RandomREDColorFades()
{
	if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	{
		unsigned int RandomREDChannel = REDArray[random(0, OneChannelTotalSum)];
		// 
		if (!tlc_isFading(RandomREDChannel))
		{
			uint16_t duration = 1000;
			uint32_t startMillis = millis() + 50;
			uint32_t endMillis = startMillis + duration;
			tlc_addFade(RandomREDChannel, 0, maxValue, startMillis, endMillis); // FADE UP
			tlc_addFade(RandomREDChannel, maxValue, 0, endMillis, endMillis + duration); // FADE DOWN
		}
	}
}


void SnakeWHITEColorFades()
{
	if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	{
		if (currentFade - startFade >= FadePeriod/250)
		{
			
			int reverseIndex = OneChannelTotalSum - WhiteArrayIndex;
			int WHITE = whiteArray[reverseIndex];
			// Serial.println(WHITE);
			if (!tlc_isFading(WHITE))
			{
				uint16_t duration = 2000;
				uint32_t startMillis = millis() + 50;
				uint32_t endMillis = startMillis + duration;
				tlc_addFade(WHITE, 0, maxValue, startMillis, endMillis);
				tlc_addFade(WHITE, maxValue, 0, endMillis, endMillis + duration);
			}
			if (WhiteArrayIndex >= OneChannelTotalSum)
			{
				WhiteArrayIndex = 0;
			}
			else {


			WhiteArrayIndex++;	
			}
			
			startFade = currentFade;
		}
	}
	// tlc_updateFades();
}

void SnakeDoubleWHITEColorFades()
{
	if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	{
		if (currentFade - startFade >= FadePeriod/7)
		{
			
			int reverseIndex = OneChannelTotalSum - WhiteArrayIndex;
			unsigned int WHITEup = whiteArray[reverseIndex];
			unsigned int WHITEdwn = whiteArray[WhiteArrayIndex];
			// Serial.println(WHITE);
			if (!tlc_isFading(WHITEup))
			{
				uint16_t duration = 2000;
				uint32_t startMillis = millis() + 50;
				uint32_t endMillis = startMillis + duration;
				tlc_addFade(WHITEup, 0, maxValue, startMillis, endMillis);
				tlc_addFade(WHITEup, maxValue, 0, endMillis, endMillis + duration);
			}
			if (!tlc_isFading(WHITEdwn))
			{
				uint16_t duration = 2000;
				uint32_t startMillis = millis() + 50;
				uint32_t endMillis = startMillis + duration;
				tlc_addFade(WHITEdwn, 0, maxValue, startMillis, endMillis);
				tlc_addFade(WHITEdwn, maxValue, 0, endMillis, endMillis + duration);
			}
			if (WhiteArrayIndex >= OneChannelTotalSum)
			{
				WhiteArrayIndex = 0;
			}
			else {
			WhiteArrayIndex++;
		}
			startFade = currentFade;
		}
	}
	// tlc_updateFades();
}

void SnakeREDColorFades()
{
	if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	{
		if (currentFade - startFade >= FadePeriod/250)
		{
			
			int reverseIndex = OneChannelTotalSum - RedArrayIndex;
			int REDChannel = REDArray[reverseIndex];

			

			if (!tlc_isFading(REDChannel))
			{
				uint16_t duration = 2000;
				uint32_t startMillis = millis() + 50;
				uint32_t endMillis = startMillis + duration;
				tlc_addFade(REDChannel, 0, maxValue, startMillis, endMillis);
				tlc_addFade(REDChannel, maxValue, 0, endMillis, endMillis + duration);
			}
			if (RedArrayIndex >= OneChannelTotalSum)
			{
				RedArrayIndex = 0;
			}
			else {
				RedArrayIndex++;	
			}
			
			startFade = currentFade;
		}
	}
	// tlc_updateFades();
}
void SnakeREDColorFadesEverySecond()
{
	if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	{
		if (currentFade - startFade >= FadePeriod/250)
		{
			
			int reverseIndex = OneChannelTotalSum - RedArrayIndex - 1;
			int REDChannel = REDArray[reverseIndex];
			if (!tlc_isFading(REDChannel))
			{
				uint16_t duration = 2000;
				uint32_t startMillis = millis() + 50;
				uint32_t endMillis = startMillis + duration;
				tlc_addFade(REDChannel, 0, maxValue, startMillis, endMillis);
				tlc_addFade(REDChannel, maxValue, 0, endMillis, endMillis + duration);
			}
			if (RedArrayIndex >= OneChannelTotalSum)
			{
				RedArrayIndex = 0;
			}
			else {
			RedArrayIndex++;
			}
			startFade = currentFade;
		}
	}
	// tlc_updateFades();
}
void SnakeORANGEColorFades()
{
	if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	{
		if (currentFade - startFade >= FadePeriod/500)
		{
			
			int reverseIndex = OneChannelTotalSum - arrayIndex;
			int Channel = REDArray[reverseIndex];
			int Channel2 = GREENarray[reverseIndex];
			uint16_t duration = 500;
			uint32_t startMillis = millis() + 50;
			uint32_t endMillis = startMillis + duration;
			if (!tlc_isFading(Channel))
			{
				tlc_addFade(Channel, 0, maxValue, startMillis, endMillis);
				tlc_addFade(Channel, maxValue, 0, endMillis, endMillis + duration);
			}
			// 
			if (!tlc_isFading(Channel2))
			{
				tlc_addFade(Channel2, 0, maxValue * 0.55, startMillis, endMillis);
				tlc_addFade(Channel2, maxValue * 0.55, 0, endMillis, endMillis + duration);
			}
			if (arrayIndex >= OneChannelTotalSum)
			{
				arrayIndex = 0;
			}
			else {
			arrayIndex++;	
			}
			
			startFade = currentFade;
		}
	}
	// tlc_updateFades();
}

void SnakePINKColorFades()
{
	if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	{
		if (currentFade - startFade >= FadePeriod/500)
		{
			
			int reverseIndex = OneChannelTotalSum - arrayIndex;
			int Channel = REDArray[reverseIndex];
			int Channel2 = BLUEarray[reverseIndex];
			
			if (!tlc_isFading(Channel))
			{
				uint16_t duration = 500;
				uint32_t startMillis = millis() + 50;
				uint32_t endMillis = startMillis + duration;
				tlc_addFade(Channel, 0, maxValue, startMillis, endMillis);
				tlc_addFade(Channel, maxValue, 0, endMillis, endMillis + duration);
				tlc_addFade(Channel2, 0, maxValue * 0.55, startMillis, endMillis);
				tlc_addFade(Channel2, maxValue * 0.55, 0, endMillis, endMillis + duration);
			}
			if (arrayIndex >= OneChannelTotalSum)
			{
				arrayIndex = 0;
			}
			else  {
				arrayIndex++;
			}
			
			startFade = currentFade;
		}
	}
	// tlc_updateFades();
}

void SnakeCYANColorFades()
{
	if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	{
		if (currentFade - startFade >= FadePeriod/500)
		{
			
			int reverseIndex = OneChannelTotalSum - arrayIndex;
			int Channel = BLUEarray[reverseIndex];
			int Channel2 = GREENarray[reverseIndex];
			if (!tlc_isFading(Channel))
			{
				uint16_t duration = 500;
				uint32_t startMillis = millis() + 50;
				uint32_t endMillis = startMillis + duration;
				tlc_addFade(Channel, 0, maxValue, startMillis, endMillis);
				tlc_addFade(Channel, maxValue, 0, endMillis, endMillis + duration);
				tlc_addFade(Channel2, 0, maxValue, startMillis, endMillis);
				tlc_addFade(Channel2, maxValue, 0, endMillis, endMillis + duration);
			}
			if (arrayIndex >= OneChannelTotalSum)
			{
				arrayIndex = 0;
			}
			else {
				arrayIndex++;
			}
			
			startFade = currentFade;
		}
	}
	// tlc_updateFades();
}
void SnakeDoubleCYANColorFades()
{
	if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	{
		if (currentFade - startFade >= FadePeriod/7)
		{
			
			int reverseIndex = OneChannelTotalSum - arrayIndex;
			unsigned int BLUEup = BLUEarray[reverseIndex];
			unsigned int GREENdn = GREENarray[arrayIndex];
			// Serial.println(WHITE);
			if (!tlc_isFading(BLUEup))
			{
				uint16_t duration = 2000;
				uint32_t startMillis = millis() + 50;
				uint32_t endMillis = startMillis + duration;
				tlc_addFade(BLUEup, 0, maxValue, startMillis, endMillis);
				tlc_addFade(BLUEup, maxValue, 0, endMillis, endMillis + duration);
			}
			if (!tlc_isFading(GREENdn))
			{
				uint16_t duration = 2000;
				uint32_t startMillis = millis() + 50;
				uint32_t endMillis = startMillis + duration;
				tlc_addFade(GREENdn, 0, maxValue, startMillis, endMillis);
				tlc_addFade(GREENdn, maxValue, 0, endMillis, endMillis + duration);
			}
			if (arrayIndex >= OneChannelTotalSum)
			{
				arrayIndex = 0;
			}

			else {
				arrayIndex++;
			}
			
			startFade = currentFade;
		}
	}
	// tlc_updateFades();
}
void SnakeRandomCYANColorFades()
{
	if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	{
		if (currentFade - startFade >= FadePeriod/500)
		{
			
			int reverseIndex = OneChannelTotalSum - arrayIndex;
			int Channel = BLUEarray[reverseIndex];
			int Channel2 = GREENarray[reverseIndex];
			int RandomValue = random(500, 4095);
			if (!tlc_isFading(Channel))
			{
				uint16_t duration = 500;
				uint32_t startMillis = millis() + 50;
				uint32_t endMillis = startMillis + duration;
				tlc_addFade(Channel, 0, RandomValue, startMillis, endMillis);
				tlc_addFade(Channel, RandomValue, 0, endMillis, endMillis + duration);
				tlc_addFade(Channel2, 0, RandomValue, startMillis, endMillis);
				tlc_addFade(Channel2, RandomValue, 0, endMillis, endMillis + duration);
			}
			if (arrayIndex >= OneChannelTotalSum)
			{
				arrayIndex = 0;
			}
			else {
				arrayIndex++;
			}
			
			startFade = currentFade;
		}
	}
	// tlc_updateFades();
}
void SnakeYELLOWColorFades()
{
	if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	{
		if (currentFade - startFade >= FadePeriod/500)
		{
			
			int reverseIndex = OneChannelTotalSum - arrayIndex;
			int Channel = REDArray[reverseIndex];
			int Channel2 = GREENarray[reverseIndex];
			uint16_t duration = 500;
			uint32_t startMillis = millis() + 50;
			uint32_t endMillis = startMillis + duration;
			if (!tlc_isFading(Channel))
			{
				tlc_addFade(Channel, 0, maxValue, startMillis, endMillis);
				tlc_addFade(Channel, maxValue, 0, endMillis, endMillis + duration);
			}
			// 
			if (!tlc_isFading(Channel2))
			{
				tlc_addFade(Channel2, 0, maxValue, startMillis, endMillis);
				tlc_addFade(Channel2, maxValue, 0, endMillis, endMillis + duration);
			}
			if (arrayIndex >= OneChannelTotalSum)
			{
				arrayIndex = 0;
			}
			else {
			arrayIndex++;	
			}
			
			startFade = currentFade;
		}
	}
	// tlc_updateFades();
}
///////////new animations, not added to IG animation


void FadeDwnWhite()
// fade up white leds before connection to gprs
{
	if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	{
		int Channel = whiteArray[WhiteArrayIndex];
		uint16_t duration = 500;
		uint32_t startMillis = millis() + 50;
		uint32_t endMillis = startMillis + duration;
		tlc_removeFades(Channel);
		int currentValue = Tlc.get(Channel);
		//tlc_addFade(Channel, currentValue, 0, startMillis, endMillis); // fade down...
		tlc_addFade(Channel, currentValue, maxValue, startMillis, endMillis); // fade UP...
		WhiteArrayIndex++; // wait for channel to finish fade, to add next one and then go to next channel
		// }
	}
	// Serial.println(arrayIndex);
	if (WhiteArrayIndex >= OneChannelTotalSum)
	{
		WhiteArrayIndex = 0;
	}
}

int BT_Count_REDColor(bool FadeDown, int BTtotal)
{
	// if (tlc_fadeBufferSize < TLC_FADE_BUFFER_LENGTH - 2)
	// {
	// at start RED channel is 0, if there is one BT, start at 0
	if (!FadeDown)
	// FADE UP
	{
		unsigned int RED;
		REDchannelsIndex = 0;
		for (REDchannelsIndex = 0; REDchannelsIndex <= BTtotal - 1; REDchannelsIndex++)
		{
			 //if (!tlc_isFading(RED))
				//{
				// RED = REDArray[REDchannelsIndex];
				// unsigned int currentValue = Tlc.get(RED);
				// uint16_t duration = 1000;
				// uint32_t startMillis = millis() + 50;
				// uint32_t endMillis = startMillis + duration;
				// tlc_addFade(RED, currentValue, maxValue, startMillis, endMillis); // FADE UP

				//}
			RED = REDArray[REDchannelsIndex];
			tlc_removeFades(RED);
			value = maxValue;
			channel = RED;
			Tlc.set(channel, value);
			Tlc.update();

		}
		FadeDown = false;
		// tlc_removeFades(RED);
		// if (Tlc.get(RED) <= 0) //check to see if this channel is lit up
		// {
		// if (!tlc_isFading(RED))
		// {
		// uint16_t duration = 1000;
		// uint32_t startMillis = millis() + 50;
		// uint32_t endMillis = startMillis + duration;
		// tlc_addFade(RED, 0, maxValue, startMillis, endMillis); // FADE UP
		// //
		// }
		// }
	}
	if (FadeDown)
	// FADE DOWNNNN
	{
		unsigned int RED;
		REDchannelsIndex = 99;
		Serial.print("BTtotal before turning down: ");
		Serial.println(BTtotal);
		for (REDchannelsIndex = 99; REDchannelsIndex >= (0 + BTtotal); REDchannelsIndex--)
		{
			
			// REDchannelsIndex = REDmax--;  //if was 91 now is 5 
			RED = REDArray[REDchannelsIndex];
			// if (REDchannelsIndex <= 0 + BTtotal) {
			// 	REDmax = 99;
			// }

			//Serial.print("RED channel index ");
			//Serial.println(REDchannelsIndex);
			if (!tlc_isFading(RED))
			{
				if (Tlc.get(RED) > 0)
				{
					// 
					unsigned int currentValue = Tlc.get(RED);
					uint16_t duration = 1000;
					uint32_t startMillis = millis() + 50;
					uint32_t endMillis = startMillis + duration;
					tlc_addFade(RED, currentValue, 0, startMillis, endMillis); // FADE DOWN
					Serial.print("Turned down RED channel: ");
					Serial.println(RED);
					Serial.print("With value: ");
					Serial.println(Tlc.get(RED));
				}
			}
		}
		FadeDown = false; // turn on fade up
	}
}

void fadeDownGB()
{
	// fade down left over green and blue channels
	// unsigned int i = 0;
	for (unsigned int i = 0; i <= OneChannelTotalSum; i++)
	{
		// Serial.println(REDchannelsIndex);
		value = 0;
		unsigned int channelB = BLUEarray[i];
		unsigned int channelG = GREENarray[i];
		if (Tlc.get(channelB) > 0)
		{
			tlc_removeFades(channelB);
			Serial.print(F("BLUE Channel was not 0 with value: "));
			Serial.println(channelB);
			Serial.println(Tlc.get(channelB));
			Tlc.set(channelB, value);
			Tlc.update();
		}
		if (Tlc.get(channelG) > 0)
		{
			tlc_removeFades(channelG);
			Serial.print(F("GREEN Channel was not 0 with value: "));
			Serial.println(channelG);
			Serial.println(Tlc.get(channelG));
			Tlc.set(channelG, value);
			Tlc.update();
		}
		// Tlc.update();
	}
}

// ///////////ANIMATION TIMER///////////////
// ///////////ANIMATION TIMER///////////////
// ///////////ANIMATION TIMER///////////////
int playAnimationTime(byte AnimationNumber)
{
	if (animStartBool)
	{
		connectAnim = true; // stop white default animations
		ANIMstartTime = ANIMcurrentTime; // set current time as animation start time
		animStartBool = false; // set bool to false, so start time is fixed
		animPlayBool = true;
		Serial.print(F("Anim play bool @anim start: "));
		Serial.println(animPlayBool);
	}
	// if (ANIMcurrentTime - ANIMstartTime >= AnimationPeriod)
	// {
	if (AnimationNumber == 0)
	{
		FadeDwnWhite();
	}
	if (AnimationNumber == 1)
	{
		RandomREDColorFades();
		playingAnim = true;
	}
	if (AnimationNumber == 2)
	{
		SnakeREDColorFades();
		playingAnim = true;
	}
	if (AnimationNumber == 3)
	{
	SnakeREDColorFadesEverySecond();
	playingAnim = true;
	}
	if (AnimationNumber == 4)
	{
		randomColorFades();
		playingAnim = true;
	}
	if (AnimationNumber == 5)
	{
		SnakeORANGEColorFades();
		playingAnim = true;
	}
	if (AnimationNumber == 6)
	{
		SnakePINKColorFades();
		playingAnim = true;
	}
	if (AnimationNumber == 7)
	{
	SnakeCYANColorFades();
	playingAnim = true;
	}
	if (AnimationNumber == 8)
	{
	SnakeRandomCYANColorFades();
	playingAnim = true;
	}
	if (AnimationNumber == 9)
	{
	SnakeDoubleCYANColorFades();
	playingAnim = true;
	}
	if (AnimationNumber == 10)
	{
	SnakeYELLOWColorFades();
	playingAnim = true;
	}

	// }
	// Serial.println(AnimationNumber);
	if ((ANIMcurrentTime - ANIMstartTime >= AnimationPeriod * 10) && (AnimationNumber > 0))
	{
		Serial.println(F("-------------------------------------------"));
		Serial.println(F("HT animation 10 sec has passed"));
		//Serial.print(F("Anim play bool after 10sec anim: "));
		//Serial.println(animPlayBool);
		animPlayBool = false; // end animation play because 10sec are passed
		BTupdateRED = true; // animation is played, so now update RED led count
		connectAnim = false; // restart to play default animations
		playingAnim = false;
	}
	if ((!playingAnim) && (ANIMcurrentTime - ANIMstartTime >= AnimationPeriod * 5) && (connectAnim))
	// connect to server
	{
		//Serial.print(F("Anim play bool before 5sec connect anim: "));
		//Serial.println(animPlayBool);
		Serial.println(F("-------------------------------------------"));
		Serial.println(F("animation 5 sec has passed CONNECT TO SERVER"));
		animPlayBool = false; // end animation play because 6sec are passed
		HTconnect = true; // set this to true to start connection to server
	}
}
