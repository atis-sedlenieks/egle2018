#include "variables.h"
// #include <MemoryFree.h>
void setup()
{
  // //BT PINS////
  pinMode(8, OUTPUT); // this pin will pull the HC-05 pin 34 (key pin) HIGH to switch module to AT mode
  pinMode(45, OUTPUT); // give power to BT HC-05
  // //BT PINS////
  // //////////////TEMP & VOLTAGE//////////////
  analogReference(INTERNAL1V1); // use the internal ~1.1volt reference, change INTERNAL to INTERNAL1V1 for a Mega
  // pinMode(Tsensor, INPUT); // TEMP PIN
  pinMode(Vsensor, INPUT); // VOLTGE PIN
  sensors.begin();
  // //////////////TEMP & VOLTAGE//////////////
  Serial.begin(9600);
  // setting up mega serial
  BTserial.begin(38400); // HC-05 default serial speed for AT mode is 38400
  // Wait for hardware to initialize
  delay(1000);
  // /////////TLC5940//////////
  Tlc.init();
  whiteArrayGEN(); // generate white LED array
  REDArrayGEN(); // generate RED LED array
  GREENarrayGEN();
  BLUEarrayGEN();
  // 
  Serial.print(F("Total sum of ONE channel: "));
  Serial.println(OneChannelTotalSum);
  Serial.print(F("Total sum of ALL channels: "));
  Serial.println(totalChannelSum);
  Serial.print(F("Total TLC FADE BUFFER: "));
  Serial.println(TLC_FADE_BUFFER_LENGTH);
  // channel = activeChannel;
  startGSM(); // starting GSM
  // /////////TLC5940//////////
  HTstartMillis = millis(); // initial HT test start time
  HTtestStartMillis = millis(); // initial HT TEST start time
  // ANIMstartTime = millis();
  // GPRSstartMillis = millis();
  startFade = millis();
  BTstartTime = millis();
  StatusStartMillis = millis();
  DefaultAnimStartMillis = millis();
  // //////GSM///////////
  OnOff(connect_path, voltage, temperature, BTmax);
}

bool firstTest = true;
bool StartBT = true;
bool playingAnim = false;
// updateStatus == true;
void loop()
{
  // get the current "time"
  StatusCurrentMillis = millis();
  HTcurrentMillis = millis();
  HTtestCurrentMillis = millis();
  ANIMcurrentTime = millis();
  BTcurrentTime = millis();
  DefaultAnimCurrentMillis = millis();
  currentFade = millis();
  // GPRScurrentMillis = millis();
  // / time
  // if (firstTest) {
  // OnOff(connect_path); // test on off state for first time;
  // }
  if (StatusCurrentMillis - StatusStartMillis >= StatusPeriod)
  {
    Serial.println(F("-------------------------------------------"));
    Serial.print(F("CHECKING ON OFF STATE "));
    client.flush();
    client.stop(); // stop existing client
    // check status every hour.
    // check ON OFF state and send temperature, voltage values
    firstTest = false;
    voltage = readBattery(); // read voltage sensor
    temperature = readTemp(); // read temperature sensor
    OnOff(connect_path, voltage, temperature, BTmax); // read ON OFF state and send data to DB
    updateStatus = true;
    StatusStartMillis = StatusCurrentMillis;
    Serial.println(F("-------------------------------------------"));
    Serial.print(F("On off status befor reset: "));
    Serial.println(OnOffstatus);
  }
  // 
  if (updateStatus == true)
  {
    // ///////////HAVE TO TURN OFF PARSER IF STATE is 0 !!!!!!!!!!!!!!!!11
    unsigned long onoff = GPRSparser(); // parse status results NOT WORKING< need to parse only results!!
    // GPRSparser();
    // Serial.println(onoff);
    OnOffstatus = onoff;
    // delay(1000);
  }
  // /
  if (OnOffstatus == 1)
  {
    if (updateStatus == true)
      //if we have positive on off status, disconect from server and change status to false.
    {
      client.stop();
      HTtotal = HTmem; // set back HT total to HTmem;
    }
    updateStatus = false;
  }
  if (OnOffstatus == 1 && StartBT == true)
  {
    // ////BT PINS /////////////
    digitalWrite(8, HIGH); // SET  this to HIGH then give power, so it goes to AT mode
    digitalWrite(45, HIGH); // give power to BT HC-05
    StartBT = false; // set this to false, so we only lunch it once.
    // ////Starting BT /////////////
    initHC05ToInq(); // Set correct states for inq
    // ////Starting BT /////////////
  }
  // /
  if ((OnOffstatus == 1) && (updateStatus == false))
  {
    // //////////////// MAIN ANIMATION LOOP ///////////////////
    updateStatus = false; // if OnOffstatus return 1, set OnOffstatus parser to false, so we can run standard program till next check
    StartBT = false;
    // update fade constantly
    tlc_updateFades();
    // update fade constantly
    // 
    // ///////////////////BT CHECK////////////////////
    // ///////////////////BT CHECK////////////////////
    // ///////////////////BT CHECK////////////////////
    BTreturn = updateBTmodule(); // calling BT update and checking BT response
    // ///////////////
    if (BTcurrentTime - BTstartTime >= BTperiod)
    // every 1sec updating BT result array for 10sec
    {
      // Serial.print("BT return: ");
      // Serial.println(BTreturn);
      BTresultArray[BTindex] = BTreturn; // add BT return items to BT results arrray then to pick highest value
      BTindex++;
      BTstartTime = BTcurrentTime;
      if (BTindex >= 10)
      // if array is full, GET MAX and do checks
      {
        // readBattery();
        // readTemp();
        // if array is full, start check for highest value to update BTtotal
        int max = 0;
        for (byte i = 1; i < 10; i++)
        {
          // Serial.println(BTresultArray[i]);
          if (BTresultArray[i] >= max)
          {
            max = BTresultArray[i];
            BTtotal = max;
          }
        }
        if (BTtotal > BTmax)
        // if BT count is bigger than BTmax count, write it to BT history, to later sand to DB
        {
          BTmax = BTtotal;
        }
        BTindex = 0;
        Serial.println(F("-------------------------------------------"));
        Serial.print(F("Total BT: before test "));
        Serial.println(BTtotal);
        Serial.print(F("BTmem: before test"));
        Serial.println(BTmem);
        BTtest = returnResult(BTmem, BTtotal, 'B');
        // ///
        if ((BTtotal > 0 && BTtest == 1) && (!animPlayBool))
        // /BT total is BIGGER
        {
          Serial.println(F("-------------------------------------------"));
          Serial.println(F("BT-TEST RESULT is BIGGER"));
          Serial.print(F("Total BT:"));
          Serial.println(BTtotal);
          animStartBool = true; // set animaton start time to this point
          animPlayBool = true; // set animation play bool to this point
          BTupdateRED = false; // set BT LED update to false, because we need to play animation first
          AnimationNumber = random(1, 4);
        }
        if ((BTtotal > 0 && BTtest == 0) && (!animPlayBool))
        // BT total count is above 0 BUT the same
        {
          FadeDown = false;
          BT_Count_REDColor(FadeDown, BTtotal);
          Serial.println(F("-------------------------------------------"));
          Serial.println(F("BT-TEST RESULT is the SAME but bigger then 0: "));
          Serial.print(F("Total BT:"));
          Serial.println(BTtotal);
        }
        if ((BTtotal >= 0 && BTtest == 2) && (!animPlayBool))
        // BT total count is SMALLER
        {
          FadeDown = true;
          BT_Count_REDColor(FadeDown, BTtotal);
          Serial.println(F("-------------------------------------------"));
          Serial.println(F("BT-TEST RESULT is SMALLER: "));
          Serial.print(F("Total BT:"));
          Serial.println(BTtotal);
        }
        if ((BTtotal == 0 && BTtest == 0) && (!animPlayBool))
        // check if there is no left RED channels on, turn them off before checking HT
        {
          FadeDown = true;
          BT_Count_REDColor(FadeDown, BTtotal);
          Serial.println(F("-------------------------------------------"));
          Serial.println(F("BT-TEST RESULT is 0 "));
          Serial.print(F("Total BT:"));
          Serial.println(BTtotal);
        }
      }
    }
    // 
    // ///////////////////BT CHECK////////////////////
    // ///////////////////BT CHECK////////////////////
    // ///////////////////BT CHECK////////////////////
    // ///
    // //////////DEFAULT ANIMATIONS////////////////////
    // //////////DEFAULT ANIMATIONS////////////////////
    if (!connectAnim)
    // DEFAULT animations, gets interrupted when connection is made
    {
      if (DefaultAnimCurrentMillis - DefaultAnimStartMillis >= DefaultAnimPeriod)
      {
        // DefaultAnim = random(1, 4);
        // /FOR color
        DefaultAnim = random(1, 11);
        // DefaultAnim = 3;
        Serial.print(F("Playing default anim: "));
        Serial.println(DefaultAnim);
        DefaultAnimStartMillis = DefaultAnimCurrentMillis;
      }
      if (DefaultAnim == 1)
      {
        RandomWhiteColorFades();
      }
      if (DefaultAnim == 2)
      {
        SnakeWHITEColorFades();
      }
      if (DefaultAnim == 3)
      {
        SnakeDoubleWHITEColorFades();
      }
      // //////color animations
      if (DefaultAnim == 4)
      {
        SnakeREDColorFadesEverySecond();
      }
      if (DefaultAnim == 5)
      {
        SnakeORANGEColorFades();
      }
      if (DefaultAnim == 6)
      {
        SnakePINKColorFades();
      }
      if (DefaultAnim == 7)
      {
        SnakeCYANColorFades();
      }
      if (DefaultAnim == 8)
      {
        SnakeDoubleCYANColorFades();
      }
      if (DefaultAnim == 9)
      {
        SnakeRandomCYANColorFades();
      }
      if (DefaultAnim == 10)
      {
        SnakeYELLOWColorFades();
      }
    }
    // //////////DEFAULT ANIMATIONS////////////////////
    // //////////DEFAULT ANIMATIONS////////////////////
    // ///////
    // ////////////UPDATE BT RED COUNT /////////////////
    if (BTupdateRED)
    {
      // animation is finished playing, now update led count
      // if BT count is larger, loop for BT count times and light up those LEDs
      // it is supossed to be larger for one time, after that, it writes in memory new BT count and test becomes 0
      FadeDown = false;
      REDchannelsIndex = 0;
      BT_Count_REDColor(FadeDown, BTtotal);
      BTupdateRED = false; // turn off update
    }
    // ////////////UPDATE BT RED COUNT /////////////////
    // 
    // ////////////// ANIMATION BEFORE SERVER CONNECTION //////////////////
    // ////////////// ANIMATION BEFORE SERVER CONNECTION //////////////////
    if ((HTcurrentMillis - HTstartMillis >= HTperiod) && (!HTconnect) && (!animPlayBool))
    {
      Serial.println(F("-------------------------------------------"));
      Serial.println(F("30 sec has passed, start connect animation"));
      animStartBool = true; // set animaton start time to this point
      animPlayBool = true; // set animation play bool to this point
      AnimationNumber = 0; // play animation before connect
      connectAnim = true; // set default animations to false and let connect animation to play
      WhiteArrayIndex = 0; // set array index to 0, so we can propperly start fade down animation
      // connectAnim = false; // set back default animations
      HTconnect = false;
      HTstartMillis = HTcurrentMillis;
      Serial.print(F("Anim play bool @ht after 30sec: "));
      Serial.println(animPlayBool);
    }
    // ////////////// ANIMATION BEFORE SERVER CONNECTION //////////////////
    // ////////////// ANIMATION BEFORE SERVER CONNECTION //////////////////
    if (HTconnect)
    // after animation connect to instagram
    {
      animPlayBool = false; // end of animation
      HTconnect = false; // connect to server one time, after this set to false and wait 30sec.
      connectAnim = false; // set back default animations
      updateHashtag(); // call HT test function one time and connect to instagram
      HTtestStart = true; // set this to true, so that HT test can be started!
      HTstartMillis = HTcurrentMillis; // correct GSM module tart time
      Serial.print(F("Anim play bool @ht connect: "));
      Serial.println(animPlayBool);
    }
    // /////// PARSE GPRS RESULTS/////////
    // /////// PARSE GPRS RESULTS/////////
    if (HTparse)
    {
      GPRSparser();
      HTtotal = hashtag_result_int;
      // Serial.println(HTtotal);
    }
    // /////// PARSE GPRS RESULTS/////////
    // /////// PARSE GPRS RESULTS/////////
    // //////////////
    // ////////TEST HT RESULTS///////////////
    // ////////TEST HT RESULTS///////////////
    if ((HTtestCurrentMillis - HTtestStartMillis >= HTperiod + 15000) && (HTtestStart))
    // every 33 sec test HT results
    {
      Serial.println(F("-------------------------------------------"));
      Serial.print(F("Total HT before test: "));
      Serial.println(HTtotal);
      Serial.print(F("Total HTmem before test: "));
      Serial.println(HTmem);
      HTtest = returnResult(HTmem, HTtotal, 'H');
      Serial.print(F("Total HTtest: "));
      Serial.println(HTtest);
      // test HT results every 10secs.
      if (HTtest == 1)
      {
        animStartBool = true; // set animaton start time to this point
        animPlayBool = true; // set animation play bool to this point
        AnimationNumber = random(4, 11);
        Serial.print(F("Playing HT animation: "));
        Serial.println(AnimationNumber);
        Serial.print(F("Anim play bool @ ht test: "));
        Serial.println(animPlayBool);
      }
      if (HTtest == 0)
      // if HT test result is the same, turn down any left oever GREEN and BLUE channels
      {
        FadeDown = true;
        BT_Count_REDColor(FadeDown, BTtotal);
        fadeDownGB();
      }
      HTtestStart = false;
      HTtestStartMillis = HTtestCurrentMillis;
    }
    // play PLAY ANIMATIONS
    if (animPlayBool)
    {
      BTupdateRED = false; // wait before update red led count
      playAnimationTime(AnimationNumber);
    }
    // play PLAY ANIMATIONS
    // 
  }
  // END OF ON LOOP
  // START OF OFF LOOP
  if ((OnOffstatus == 0) && (updateStatus == true))
  {
    // turn everything OFF
    Tlc.clear();
    Tlc.update();
    client.stop();
    digitalWrite(8, LOW); // SET  this to LOW then give power, so it goes to AT mode
    digitalWrite(45, LOW); // turn down power to BT HC-05
    StartBT = true; // set BT to true and wait for OnOffstatus == 1
    updateStatus == false; // so we can stop parser.
  }
  // END OF OFF LOOP
}

// edn of LOOP
int returnResult(unsigned long oldResult, unsigned long newResult, char BTorHT)
// test function for BT and HT results
{
  // /////update memory before testing results///////
  switch (BTorHT)
  // which memory to update? depending of which is tested!
  {
    case 'B':
    // do something
    BTmem = BTtotal; // write new BT result to BT memory variable
    break;
    case 'H':
    // do something
    HTmem = HTtotal; // write new HT result to HT memory variable
    break;
    default:
    // do something
    break;
  }
  // ///test results//////
  if (newResult == oldResult)
  {
    // Serial.println(F("new result is EQUAL"));
    return 0;
  }
  if (newResult < oldResult)
  {
    // Serial.println(F(" new result is SMALLER"));
    return 2;
  }
  if (newResult > oldResult)
  // && testBool true
  {
    return 1;
  }
}
