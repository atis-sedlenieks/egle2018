//////////////////GPRS/////////////////////
// libraries
#include <GSM.h>
// PIN Number
#define PINNUMBER "1843"
#define GPRS_APN 0 // replace your GPRS APN
#define GPRS_LOGIN 0 // replace with your GPRS login
#define GPRS_PASSWORD 0 // replace with your GPRS password
// initialize the library instance
GSMClient client;
GPRS gprs;
GSM gsmAccess;
// //CSTRING

//// STATUS////
//char Status_path[] = "/egle/status.php";
//char connect_path[50];
unsigned long StatusStartMillis;
unsigned long StatusCurrentMillis;
const unsigned long StatusPeriod = 3600000; //60min //120000; //2min  //600000; //10min 
unsigned long OnOffstatus = 0;
bool updateStatus = true;
char connect_path[] = "/egle/status.php";
//char complete_path[] = "/egle/status.php?voltage=" +voltage+"&temp="+tempC+"&BT="+BTmax;
char complete_path[50];
//http://www.atis-sedlenieks.com/egle/status.php?voltage=1.15&temp=1.30&BT=10
char cP = ' ';

///////STATUS//////////

// variable for save response obtained
bool poundSignReceived = false;
bool fullDataReceived = false;
bool HTconnect = false;
bool connectAnim = false;
bool HTtestStart = false;
int HTtest;
bool HTparse = false;
unsigned long HTtotal;
unsigned long hashtag_result_int;

unsigned long HTmem; // previous hashtag result, to compare with new result
bool testBool = true;
bool writeBool = false;
char hashtag_buffer[200];
byte HTindex = 0; //hashtag counter index
//unsigned long APIcheckCount = 0;
//unsigned long delayBetweenChecks = 20000; // mean time between api requests
//unsigned long whenDueToCheck = 0;
char cHT = ' ';

unsigned long GPRSstartMillis;  
unsigned long GPRScurrentMillis;
const unsigned long GPRSPeriod = 30000; // the time after connect to server again 30sec
///////////
unsigned long HTstartMillis; 
unsigned long HTcurrentMillis;
const unsigned long HTperiod = 60000;  // call HT hashtag connect and HT test every 30sec
unsigned long HTtestCurrentMillis;

unsigned long HTtestStartMillis;
// Inputs
String hashtag = "lv100egle";
char server[] = "atis-sedlenieks.com";
char path[] = "/egle/instagram-tag-parser.php?insta_hashtag=";
byte port = 80; // port 80 is the default for HTTP
//////////////////GPRS/////////////////////
//////////////////GPRS/////////////////////
//////////////////GPRS/////////////////////
//////////////////GPRS/////////////////////
//////////////////GPRS/////////////////////
//////////////////GPRS/////////////////////

//////////////BT variables///////////////////
//////////////BT variables///////////////////
//////////////BT variables///////////////////
//////////////BT variables///////////////////
//////////////BT variables///////////////////
HardwareSerial & BTserial = Serial1; //SETTING SERIAL1 
//	BTrx --> 18tx1 sending data to BT orange wire
//	BTtx --> 19rx1 recieving data from BT green wire
int BTtest;
char c = ' ';
char lineBuffer[100]; //aray to buffer Bt names
byte index = 0;
byte BTtotal;
byte BTresult;
byte BTmem;
byte BTreturn; 
unsigned int BTmax; //memory of max BT count in session to send to DB
int BTresultArray[10]; //array for BT return results
byte BTindex = 0; //BT result array index

//bool updateBT = false;

unsigned long BTstartTime; 
unsigned long BTcurrentTime;
const unsigned long BTperiod = 500;  // update BT and HT results in main loop
//////////////BT variables///////////////////

/////////////////ANIMATIONS////////////////////////
/////////////////ANIMATIONS////////////////////////
/////////////////ANIMATIONS////////////////////////
/////////////////ANIMATIONS////////////////////////
/////////////////ANIMATIONS////////////////////////
/////////////////ANIMATIONS////////////////////////
/////////////////ANIMATIONS////////////////////////
/////////////////ANIMATIONS////////////////////////
#include <Tlc5940.h>
#include "tlc_fades.h"
TLC_CHANNEL_TYPE channel;
//
byte OneChannelTotalSum = ((NUM_TLCS * 16) /4)-1; // how many white, red, green or blue channels there is? 0--99 = 100
unsigned int totalChannelSum = (NUM_TLCS * 16) -1; // total sum of all channels
//
bool update = true;
unsigned int maxValue = 4095;
byte activeChannel = 3; // 0-green; 1-blue; 2-red; 3-white
//
unsigned int whiteArray[100]; // /arrray of WHITE channel LEDs
byte WhiteArrayIndex = 0;
//
unsigned int REDArray[100]; //arrray of RED channel LEDs
byte RedArrayIndex = 0;
int REDchannelsIndex = 0; // counting how many BT there are in FOR loop.
int REDmax = 99;
//
unsigned int GREENarray[100];
byte GreenArrayIndex = 0;
//
unsigned int BLUEarray[100];
byte BlueArrayIndex = 0;
//
unsigned int arrayIndex = 0; //array index
//
byte AnimationNumber = 0; // animations numbers for animation play function
byte DefaultAnim = 1;
//
byte directionChannel = 4;
byte valueAddition = 100;
unsigned int value = 0;
//
unsigned long startFade; 
unsigned long currentFade;
const unsigned long FadePeriod = 1000;  //fade update interval
//
unsigned long ANIMstartTime;  
unsigned long ANIMcurrentTime;
const unsigned long AnimationPeriod = 1000; // the time after update animations in main loop
unsigned long DefaultAnimStartMillis;
unsigned long DefaultAnimCurrentMillis;
const unsigned long DefaultAnimPeriod = 10000;
//
bool animPlayBool = false;
bool animStartBool = false;
bool update1 = true;
bool FadeDown = false; // turn on only when want to fade down cahnnel
bool BTupdateRED = false; // is it time to update RED leds as BT count.
///////ANIMATIONS/////////
///
////////TEMP & VOLTAGE METER///////////
// //////////////TEMP////////////
// //////////////TEMP////////////
#include <OneWire.h>
#include <DallasTemperature.h>
// Data wire is conntec to the Arduino digital pin 4
#define ONE_WIRE_BUS 4
// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature sensor 
DallasTemperature sensors(&oneWire);

unsigned int totalTemp;
//const int Tsensor = A5; // Assigning analog pin A5 to variable 'sensor'
float vout; // temporary variable to hold sensor reading
float tempC;
float temperature;

// //////////////TEMP////////////
// //////////////TEMP////////////
///////////VOLTGE/////////////
///////////VOLTGE/////////////
/*
0 - ~17volt voltmeter
works with 3.3volt and 5volt Arduinos
uses the internal 1.1volt reference
150k resistor from A1 to +batt
10k resistor from A1 to ground
optional 100n capacitor from A1 to ground for stable readings
*/
//float Aref = 1.063; // change this to the actual Aref voltage of ---YOUR--- Arduino, or adjust to get accurate voltage reading (1.000- 1.200)
float Aref = 1.085;
const int Vsensor = A1;
unsigned int totalVolt; // A/D output
float voltage; // converted to volt

/////////////////////

  
