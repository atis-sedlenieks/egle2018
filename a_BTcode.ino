void delayAndRead()
{
	delay(50);
	while (BTserial.available())
	{
		char c = BTserial.read();
		Serial.print(c);
	}
	delay(800);
}

void initHC05ToInq()
{
  Serial.println(F("Starting BT module..."));
	BTserial.println("AT+CMODE=1"); // Enable connect to any device
	delayAndRead();
	BTserial.println("AT+ROLE=1"); // Set to master in order to enable scanning
	delayAndRead();
  BTserial.println("AT+INQM=0,100,12"); // RSSI, Max 100 devices, ~61sec = 48
	delayAndRead();
	BTserial.println("AT+CLASS=0"); // Disable COD filter
	delayAndRead();
	BTserial.println("AT+INIT"); // Init.
	delayAndRead();
  BTserial.println("AT+INQ");// start inq

}

 int updateBTmodule() {
  // Keep reading from HC-05 and send to Arduino Serial Monitor
  if (BTserial.available())
  {
    // Read character and append into buffer
    c = BTserial.read();
    lineBuffer[index] = c;
    index++;
    // When line ends
    if (c == '\n')
    {
      // Reset buffer index for next line
      index = 0;
      BTresult++;
      if (lineBuffer[0] == 'O' && lineBuffer[1] == 'K')
      {
        BTserial.println("AT+INQ");
        BTresult = 0;
      }
     
    }
  }

  return BTresult;
}
